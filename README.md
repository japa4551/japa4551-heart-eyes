And why would you read me?

[Camera+ highly recommended](https://steamcommunity.com/sharedfiles/filedetails/?id=867467808)

Adds multiple Heart-Shaped Pupils/Eyes Genes:
- Pink
- Purple
- Yellow
- Red
- Light Blue
- Green

Why:
- Why not?

[Steam Workshop Page](https://steamcommunity.com/sharedfiles/filedetails/?id=3195274852)

### Requirements:
- [RimWorld - Biotech (DLC)](https://store.steampowered.com/app/1826140/RimWorld__Biotech/)

### Special thanks to [Lazy Friday Studio](https://steamcommunity.com/sharedfiles/filedetails/?id=2922457045) for allowing me to use their Textures as a base.